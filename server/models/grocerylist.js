module.exports = function(connection, Sequelize){
    // grocerylist this have to match the mysql table
    // return Grocerylist object is used within the JS
    var Grocerylist = connection.define('grocery_lists', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        upc12: {
            type: Sequelize.BIGINT(12),
            allowNull: false
        },
        brand: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name:{
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        timestamps: false
    });
    return Grocerylist;
}