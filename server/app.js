/**
 * Server side code.
 */
"use strict";
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var moment = require('moment');
var offset = moment().utcOffset();
var finalOffset = ''.concat(offset < 0 ? "-" : "+",moment(''.concat(Math.abs(offset/60),Math.abs(offset%60) < 10 ? "0" : "",Math.abs(offset%60)),"hmm").format("HH:mm"));

var Sequelize = require("sequelize");

const NODE_PORT = process.env.PORT || 3000;
const GROCERY_API = '/api/grocery'
const SQL_USERNAME="root";
const SQL_PASSWORD="lt91889088"
const Op = Sequelize.Op

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + "/../client/"));

var connection = new Sequelize(
    'grocerydb',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        dialectOptions: {
            useUTC: false //for reading from database
        },
        timezone: finalOffset, //for writing to database
        pool: {
            max: 10,
            min: 0,
            idle: 20000,
            acquire: 20000
        }
    }
);

var Grocerylist = require('./models/grocerylist')(connection, Sequelize);


//Database Functions
// retrieve via GET
app.get(GROCERY_API, (req, res)=>{
    console.log("GET result:  " + req.query);
    var brand_name = getOrDefault(req.query.brand);
    var product_name = getOrDefault(req.query.product);
    var sortbyBrand = getOrDefault(req.query.sortbyBrand, 'ASC');
    var sortbyProduct = getOrDefault(req.query.sortbyProduct, 'ASC');
    var upc = getOrDefault(req.query.upc);
    var id = getOrDefault(req.query.id);

    var itemsPerPage = parseInt(req.query.itemsPerPage) || 20;
    var currentPage = parseInt(req.query.currentPage) || 1;

    var offset = (currentPage - 1) * itemsPerPage;
    console.log('Offset: ' + offset);


    console.log('brand_name :' + brand_name);
    console.log('product_name :' + product_name);
    console.log('itemsPerPage :' + itemsPerPage);
    console.log('currentPage :' + currentPage);
    console.log("sortby :" + sortbyBrand + ' ' + sortbyProduct);

    //construct WHERE condition
    var whereClause = { offset: offset, limit: itemsPerPage, order: [['brand', sortbyBrand], ['name', sortbyProduct]], where: {}};
   
    if (!isEmpty(id)) {
        //only id
        whereClause.where.id = id;
    } else if (!isEmpty(upc)) {
        // only upc
        whereClause.where.upc12 = upc;
    } else {
        // otherwise can be brand or product
        if (!isEmpty(brand_name)) {
            whereClause.where.brand = {
                [Op.like]: '%' + brand_name + '%'
              } ;
        };
        if (!isEmpty(product_name)) {
            whereClause.where.name = {
                [Op.like]: '%' + product_name + '%'
              } ;
        };
    }
    ;
    console.log(whereClause);

    Grocerylist.findAndCountAll(whereClause).then((results)=>{
        res.status(200).json(results);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

// create via POST
app.post(GROCERY_API, (req, res)=>{
    console.log("SERVER RECEIVED >>> " + JSON.stringify(req.body));
    var groceryItem = req.body;

    Grocerylist.create(groceryItem).then((result)=>{
        console.log('Save Success ' + result);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });
    
});

//update via PUT
app.put(GROCERY_API, (req, res)=>{
    console.log("SERVER RECEIVED >>> " + JSON.stringify(req.body));
    var groceryItem = req.body;

    var whereClause = {limit: 1, where: {id: groceryItem.id}};
    console.log('UPDATE WHERE clause: ' + whereClause);

    Grocerylist.findOne(whereClause).then((result)=>{
        console.log('Update Success ' + result);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });   
    
});


//DELETE via delete
app.delete(GROCERY_API+"/:id", (req, res)=>{
    console.log('delete Param: ' + req.params.id);
    var id = req.params.id;
    
    var whereClause = {limit: 1, where: {id: id}};
    console.log('DELETE whereClause' + whereClause);

    Grocerylist.findOne(whereClause).then((result)=>{
        console.log('DESTROYING ' + JSON.stringify(result));
        result.destroy();
        res.status(200).json({});
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    }); 
});

//endpoint to provide information to the client on the total number of questions

app.use(function (req, res) {
    res.send("<h1>!!!! Page not found ! ! !</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//helper functions
function isEmpty(str) {
   //check if str is empty including undefined and null
   return (str === undefined || str === null || str === "" || str === 'undefined' || str === 'null');
};

function getOrDefault(str, defaultVal) {
    //returns the default value input is empty

    var defaultVal = defaultVal || '';

    if (isEmpty(str)) {
        console.log ('isEmpty: ' + str + ' using Default: ' + defaultVal)
       return  defaultVal;
    } else {
        console.log ('NOT isEmpty: ' + str);
       return  str;
    }
}

//make the app public. In this case, make it available for the testing platform
module.exports = app

