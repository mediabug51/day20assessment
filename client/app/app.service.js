(function(){
    angular
        .module("myApp")
        .service("GMSAppAPI", [
            '$http',
            GMSAppAPI
        ]);
        
    var currRecord = {}
    function GMSAppAPI($http){
        var self = this;
        self.searchGrocery =  searchGrocery;
        self.addGrocery = addGrocery;
        self.deleteGrocery = deleteGrocery;
        self.loadGroceryById = loadGroceryById;
        self.loadGroceryByUpc = loadGroceryByUpc;
        

        // query string
        function searchGrocery (brand, product, sortbyBrand, sortbyProduct, itemsPerPage, currentPage){
            return $http.get(`/api/grocery?brand=${brand}&product=${product}&sortbyBrand=${sortbyBrand}&sortbyProduct=${sortbyProduct}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        // query by ID
        function loadGroceryById(id){
            return $http.get(`/api/grocery?id=${id}`);
        }

        // query by loadGroceryByUpc
        function loadGroceryByUpc(upc){
            return $http.get(`/api/grocery?upc=${upc}`);
        }

        // post string
        function addGrocery (newItem){
            return $http.post('/api/grocery', newItem);
        }

        // delete string
        function deleteGrocery (itemId){
            return $http.delete('/api/grocery/' + itemId);
        }

       
    }
})();