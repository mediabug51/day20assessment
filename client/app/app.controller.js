/**
 * Client side code.
 */
(function () {
    "use strict";
    angular.module("myApp")
    .controller("groceryListController", groceryListController)
    .controller("groceryEditController", groceryEditController)
    
    groceryListController.$inject = ["GMSAppAPI", "$http", "$state", "$stateParams", "$rootScope", "$scope"];
    groceryEditController.$inject = ["GMSAppAPI", "$http", "$state", "$stateParams", "$rootScope", "$scope"];
    
   
 

 

    function groceryListController(GMSAppAPI, $http, $state, $stateParams, $rootScope, $scope) {
        var self = this; // vm
        self.searchGrocery = searchGrocery;
        self.addGrocery = addGrocery;
        self.deleteGrocery = deleteGrocery;
        self.pageChanged = pageChanged;

        $scope.$on("refreshResultList",function(){
            console.log("refresh  list ");
            searchGrocery()
         });

        function init() {
            self.brand = '';
            self.product = '';
            self.sortby = '';
            self.sortbyBrand = '';
            self.sortbyProduct = '';
            self.itemsPerPage = 20; 
            self.currentPage = 1;
            
        }

        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchGrocery();
            console.log($scope.numPages);
        }

        function searchGrocery() {
            var searchPromise = GMSAppAPI.searchGrocery(self.brand, self.product, self.sortbyBrand, self.sortbyProduct, self.itemsPerPage, self.currentPage);
             
            searchPromise.then(function(result){
              console.log('searchPromise Trigger Result: ' + JSON.stringify( result));
    
              self.totalItems = result.data.count;
              self.groceryList = result.data.rows;     
              
            }, function(err){
               alert('Error encountered at searchPromise: ' + JSON.stringify( err));
            }).catch((error)=>{
             console.log(error);
              });
         }

        function addGrocery() {

            GMSAppAPI.loadGroceryByUpc(self.currRecord.upc12).then(function(result){
                console.log('VALIDATION: ' + JSON.stringify(result))

                if (result.data.count > 0) {
                    alert('Sorry! Data already existed!');
                } else {
                    // data is new
                    var searchPromise = GMSAppAPI.addGrocery(self.currRecord)
                    
                     searchPromise.then(function(result){
                       console.log('addPromise Trigger Result: ' + JSON.stringify( result));
                       alert('Record Saved');
                       $state.go('home');
         
                     }, function(err){
                        alert('Error encountered at searchPromise: ' + JSON.stringify( err));
                     }).catch((error)=>{
                         console.log(error);
                          });

                }
            });

         }
        
         function deleteGrocery(itemId) {
            GMSAppAPI.deleteGrocery(itemId).then(function(result) {
                console.log('DELETE SUCCESS: ' + JSON.stringify(result) );
                $rootScope.$broadcast('refreshResultList');
            }, function(err){
                alert('Error encountered at deleteGrocery: ' + JSON.stringify( err));
             }).catch((error)=>{
                console.log(error);
                 });
         }
  
        
        init();
    }

    function groceryEditController(GMSAppAPI, $scope, $state, $stateParams, $rootScope) {
      var self = this;
      var idItem = $stateParams.id;

      self.saveUpdate = saveUpdate;

      function saveUpdate(){
        alert('Saving Update ' + JSON.stringify(self.currRecord))
        var searchPromise = GMSAppAPI.updateGrocery(self.currRecord).then(function(result){
            alert('Record Updated');
            $state.go('home');
            $rootScope.$broadcast('refreshResultList');
        });
      }
      GMSAppAPI.loadGroceryById(idItem).then(function(result) {
        if (result.data.count>=1) {
            console.log('Found ' + JSON.stringify(result.data.rows[0]));
            self.currRecord = result.data.rows[0];
            
        } else {
            alert('Error Loading Record from DB');
        }
        // 
    })
    }

})();