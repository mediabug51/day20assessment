/**
 * Client side code.
 */
(function () {
    "use strict";
    angular.module("myApp", ['ui.bootstrap', 'ui.router']);
})();