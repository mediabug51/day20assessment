(function () {  
    
    angular
    .module("myApp")
    .config(uirouterAppConfig);

    uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];
    
    
    function uirouterAppConfig($stateProvider, $urlRouterProvider){
    
    $stateProvider
        .state("home",{ //localhost:3000/#!/A 
            url : '/home',
            templateUrl: "subpages/home.html",
            controller : 'groceryListController', //Ok to use different controller (1 page 1 controller)
            controllerAs : 'ctrl'
        })
        .state("editItem", {
            url: "/edit/:id",
            templateUrl: "subpages/editItem.html",
            controller : 'groceryEditController',
            controllerAs : 'ctrl'
        })
        .state("addItem", {
            url: "/addItem",
            templateUrl: "subpages/addItem.html",
            controller : 'groceryListController',
            controllerAs : 'ctrl'
        })
        $urlRouterProvider.otherwise("/home"); //if can't find any route, it goes to A 
    
    }
    
    })();